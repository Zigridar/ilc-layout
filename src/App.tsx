import React from 'react';
import './styles/fonts.scss';
import './styles/index.scss';
import { Main } from './components/frames/main/Main.tsx';
import { ClientsBlock } from './components/frames/clients/ClientsBlock.tsx';
import { Advantages } from './components/frames/advantages/Advantages.tsx';
import { Tariffs } from './components/frames/tariffs/tariffs.tsx';
import { Offer } from './components/frames/offer/Offer.tsx';
import { Director } from './components/frames/director/Director.tsx';
import { Header } from './components/header/Header.tsx';
import { Footer } from './components/footer/Footer.tsx';

export const App: React.FC = () => (
  <>
    <Header />
    <Main />
    <ClientsBlock />
    <Advantages />
    <Tariffs />
    <Offer />
    <Director />
    <Footer />
  </>
);
