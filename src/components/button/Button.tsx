import React from 'react';
import styles from './button.module.scss';
import cn from 'classnames';

type Props = {
  children: string;
} & IClassName;

export const Button: React.FC<Props> = ({ children, className }) => (
  <button className={cn(styles.button, className)}>{children}</button>
);
