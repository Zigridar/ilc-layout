import React from 'react';
import cn from 'classnames';
import logo from '../../assets/images/logo.svg';
import vk from '../../assets/images/vk.svg';
import instagram from '../../assets/images/instagram.svg';
import twitter from '../../assets/images/twitter.svg';
import facebook from '../../assets/images/facebook.svg';
import styles from './footer.module.scss';

type Card = {
  logo: string;
};

const cards: Array<Card> = [
  {
    logo: vk,
  },
  {
    logo: twitter,
  },
  {
    logo: instagram,
  },
  {
    logo: facebook,
  },
];

export const Footer: React.FC<IClassName> = ({ className }) => (
  <footer className={cn(styles.container, className)}>
    <div className={styles.top}>
      <div className={styles.logo}>
        <img src={logo} alt='logo' />
        <p>Первый цифровой международный юридический центр</p>
      </div>
      <div className={styles.social}>
        {cards.map(({ logo }) => (
          <a href='#' className={styles.item} key={logo}>
            <img src={logo} alt='logo' />
          </a>
        ))}
      </div>
    </div>
    <div className={styles.bottom}>
      <p className={styles.left}>
        ©2021 «International Legal Center» | Разработано: effex-it.com
      </p>
      <div className={styles.right}>
        <a href='#'>Политика конфиденциальности</a>
        <a href='#'>Пользовательское соглашение</a>
      </div>
    </div>
  </footer>
);
