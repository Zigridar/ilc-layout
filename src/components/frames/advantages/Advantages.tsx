import React from 'react';
import styles from './advantages.module.scss';
import { Phone } from './components/phone/Phone.tsx';
import { Sirius } from './components/sirius/Sirius.tsx';
import { Text } from './components/text/Text.tsx';
import { Cards } from './components/cards/Cards.tsx';

export const Advantages: React.FC = () => (
  <section className={styles.container}>
    <h2 className={styles.title}>Преимущества сервиса</h2>
    <Phone className={styles.phone} />
    <Sirius />
    <Text />
    <Cards className={styles.cards} />
  </section>
);
