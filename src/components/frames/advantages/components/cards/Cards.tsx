import React from 'react';
import chat from '../../../../../assets/images/chat.svg';
import money from '../../../../../assets/images/money.svg';
import calc from '../../../../../assets/images/calc.svg';
import access from '../../../../../assets/images/access.svg';
import docs from '../../../../../assets/images/docs.svg';
import styles from './cards.module.scss';
import cn from 'classnames';

type Card = {
  img: string;
  title: string;
  text: string;
};

const cards: Array<Card> = [
  {
    title: 'Удобный формат',
    text: 'Консультируем 24/7 в любом удобном формате: чат, аудио-звонок, c помощью искусственного интеллекта Sirius в личном кабинете.',
    img: chat,
  },
  {
    title: 'Доступная цена',
    text: 'Выбирайте комфортный для себя пакет юридической поддержки, оплачивая только то, что действительно нужно.',
    img: money,
  },
  {
    title: 'Налоговый калькулятор',
    text: 'Точный расчет налоговой нагрузки за считанные минуты с помощью телеграм-бота ILC_tax — это быстро и удобно!',
    img: calc,
  },
  {
    title: 'Конструктор документов',
    text: 'Доступ к более чем 9500 готовых и проработанных документов для бизнеса — бесплатно!',
    img: docs,
  },
  {
    title: 'Бесплатный доступ для всех, кто зарегистрируется ',
    text: 'Конструктор с ограниченным количеством документов на базе телеграм-бота',
    img: access,
  },
];

export const Cards: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.container, className)}>
    {cards.map(({ img, text, title }) => (
      <div key={title} className={styles.card}>
        <img src={img} alt='logo' />
        <h4 className={styles.title}>{title}</h4>
        <p className={styles.text}>{text}</p>
      </div>
    ))}
  </div>
);
