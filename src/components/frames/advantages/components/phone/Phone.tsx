import React from 'react';
import cn from 'classnames';
import styles from './phone.module.scss';
import smart from '../../../../../assets/images/smart-sirius.png';
import { Button } from '../../../../button/Button.tsx';

export const Phone: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.container, className)}>
    <div className={styles.imageBlock}>
      <img src={smart} alt='smart-sirius' />
      <div className={styles.help}>
        <p>Чем могу вам помочь?</p>
        <Button className={styles.btn}>Задайте вопрос</Button>
      </div>
    </div>
  </div>
);
