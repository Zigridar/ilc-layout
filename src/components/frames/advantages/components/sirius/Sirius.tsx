import React from 'react';
import cn from 'classnames';
import styles from './sirius.module.scss';

export const Sirius: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.container, className)}>
    <h3 className={styles.title}>SIRIUS</h3>
    <p className={styles.text}>
      — первый полноценный искусственный интеллект, самостоятельно
      консультирующий людей по любым юридическим вопросам в режиме реального
      времени.
    </p>
    <p className={styles.text}>
      Sirius анализирует входящий запрос и оперативно генерирует
      квалифицированный ответ на основании юридической логики.
    </p>
    <p className={styles.text}>
      Доступ к системе предоставляется владельцу полиса в личном кабинете на
      нашем сайте.
    </p>
  </div>
);
