import React from 'react';
import cn from 'classnames';
import styles from './text.module.scss';
import { Triangle } from '../../../../triangle/Triangle.tsx';

const items = [
  'работает 24/7',
  '100% точность и корректность ответов',
  'при необходимости, подключает живого специалиста',
  'не берет больничный и никогда не уволится',
];

export const Text: React.FC<IClassName> = ({ className }) => (
  <ul className={cn(styles.container, className)}>
    {items.map(item => (
      <li key={item}>
        <span className={styles.triangle}>
          <Triangle />
        </span>
        <span>{item}</span>
      </li>
    ))}
  </ul>
);
