import React from 'react';
import styles from './clients.module.scss';
import { Text } from './components/text/Text.tsx';
import { Cards } from './components/cards/Cards.tsx';
import { Clients } from './components/clients/Clients.tsx';

export const ClientsBlock: React.FC = () => (
  <section className={styles.container}>
    <Text className={styles.text} />
    <Cards className={styles.cards} />
    <Clients className={styles.clients} />
  </section>
);
