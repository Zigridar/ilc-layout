import React from 'react';
import styles from './cards.module.scss';
import cn from 'classnames';

export const Cards: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.cards, className)}>
    <div className={styles.topLeft}>
      <p className={styles.gradient}>
        100+<span>тыс</span>
      </p>
      <p className={styles.text}>Клиентов по всему миру</p>
    </div>
    <div className={styles.topRight}>
      <p className={styles.gradient}>
        15<span>лет</span>
      </p>
      <p className={styles.text}>Опытных юристов в штате</p>
    </div>
    <div className={styles.bottomLeft}>
      <p className={styles.gradient}>50+</p>
      <p className={styles.text}>Опытных юристов в штате</p>
    </div>
    <div className={styles.bottomRight}>
      <p className={styles.gradient}>SIRIUS</p>
      <p className={styles.text}>Система на базе искусственного интеллекта</p>
    </div>
  </div>
);
