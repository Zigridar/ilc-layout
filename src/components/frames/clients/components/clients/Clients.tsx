import React from 'react';
import cn from 'classnames';
import styles from './clients.module.scss';
import family from '../../../../../assets/images/family.png';
import startup from '../../../../../assets/images/startup.png';
import team from '../../../../../assets/images/team.png';

type CardDescriptor = {
  image: string;
  text: string;
  title: string;
};

const cards: Array<CardDescriptor> = [
  {
    title: 'Бизнесу',
    text: 'Услуги профессиональных юристов для вашей компании по фиксированному тарифу — это в несколько раз выгоднее, чем содержать свой штат.',
    image: team,
  },
  {
    title: 'StartUP проектам',
    text: 'Эксперты ILC помогут выбрать оптимальную правовую форму для ведения бизнеса, оптимизировать налоги и подготовить необходимый пакет документов.',
    image: startup,
  },
  {
    title: 'Частным клиентам',
    text: 'Оперативные решения сложных вопросов в режиме онлайн: ДТП, наследство, страховые споры, земельные и имущественные процессы, защита прав.',
    image: family,
  },
];

export const Clients: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.clients, className)}>
    <h3 className={styles.header}>Даём поддержку</h3>
    <div className={styles.cards}>
      {cards.map(({ title, image, text }) => (
        <div key={title} className={styles.card}>
          <img src={image} alt='image' />
          <h4 className={styles.title}>{title}</h4>
          <p className={styles.text}>{text}</p>
        </div>
      ))}
    </div>
  </div>
);
