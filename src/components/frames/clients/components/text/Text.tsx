import React from 'react';
import cn from 'classnames';
import styles from './text.module.scss';
import logo from '../../../../../assets/images/logo-without-text.svg';

export const Text: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.text, className)}>
    <img src={logo} alt='logo' />
    <h2 className={styles.title}>Ваша личная online команда юристов</h2>
    <p className={styles.contentText}>
      Мы предоставляем комплексный консалтинговый сервис с полисной системой
      обслуживания для частных клиентов и для бизнеса, с использованием новейших
      цифровых технологий.
    </p>
  </div>
);
