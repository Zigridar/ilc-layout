import React from 'react';
import styles from './direcor.module.scss';
import { Header } from './components/header/Header.tsx';
import { Hero } from './components/hero/Hero.tsx';

export const Director: React.FC = () => (
  <section className={styles.container}>
    <Header />
    <Hero className={styles.hero} />
  </section>
);
