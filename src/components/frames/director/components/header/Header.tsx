import React from 'react';
import cn from 'classnames';
import styles from './header.module.scss';

type Card = {
  title: string;
  text: string;
};

const cards: Array<Card> = [
  {
    title: '$2 млн.',
    text: 'Заработают партнеры ILC',
  },
  {
    title: '10 000+',
    text: 'Полисов будет оформлено по партнерским ссылкам',
  },
  {
    title: '800+',
    text: 'Партнеров присоединится к нам',
  },
  {
    title: '$1500',
    text: 'Средний доход партнера в месяц',
  },
];

export const Header: React.FC<IClassName> = ({ className }) => (
  <div className={styles.wrap}>
    <div className={cn(styles.container, className)}>
      <h2 className={styles.title}>
        Прогнозы партнерской программы на 2021 год
      </h2>
      <div className={styles.cards}>
        {cards.map(({ text, title }) => (
          <div key={title} className={styles.card}>
            <p className={styles.label}>{title}</p>
            <p className={styles.text}>{text}</p>
          </div>
        ))}
      </div>
    </div>
  </div>
);
