import React from 'react';
import cn from 'classnames';
import styles from './hero.module.scss';
import manager from '../../../../../assets/images/manager.png';
import { Button } from '../../../../button/Button.tsx';

export const Hero: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.container, className)}>
    <div className={styles.manager}>
      <img src={manager} alt='manager' />
      <div className={styles.quote}>
        <p>
          Я лично буду поддерживать вас на всех этапах развития в программе.
          Обращайтесь ко мне с любым вопросом email.
        </p>
      </div>
    </div>
    <div className={styles.card}>
      <p className={styles.post}>Руководитель ООО ILC</p>
      <h2 className={styles.name}>Наталья Гуторова</h2>
      <p className={styles.experience}>10 лет в управлении и юриспуденции</p>
      <div className={styles.description}>
        <p>
          Мы стараемся окружить заботой и создать максимально комфортные условия
          для развития каждого партнера:
        </p>
        <ul className={styles.list}>
          <li>личный наставник,</li>
          <li>пошаговое обучение,</li>
          <li>
            готовые инструменты для продвижения и мгновенные выплаты бонусов.
          </li>
        </ul>
      </div>
      <p className={styles.end}>
        Больше деталей партнерской программы смотрите в личном кабинете после
        регистрации.
      </p>
      <Button className={styles.btn}>Зарегистрироваться</Button>
    </div>
  </div>
);
