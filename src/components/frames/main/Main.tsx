import React from 'react';
import styles from './main.module.scss';
import { Circle } from './components/circle/Circle.tsx';
import creative from '../../../assets/images/creative-process.png';
import { Advantages } from './components/advantages/Advantages.tsx';
import { TextBlock } from './components/text-block/Text.tsx';

export const Main: React.FC = () => (
  <section className={styles.container}>
    <TextBlock className={styles.textBlock} />
    <div className={styles.imageWrapper}>
      <img src={creative} className={styles.creative} alt='creative' />
    </div>
    <Circle className={styles.circle} />
    <Advantages className={styles.advantages} />
  </section>
);
