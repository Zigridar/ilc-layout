import React from 'react';
import styles from './advantages.module.scss';
import dollars from '../../../../../assets/images/dollars.svg';
import advantages from '../../../../../assets/images/advantages.svg';
import time from '../../../../../assets/images/time.svg';
import cn from 'classnames';

const data: Array<Props> = [
  {
    text: 'Юридическая поддержка 24/7',
    img: time,
  },
  {
    text: 'Множество возможностей в одном полисе',
    img: advantages,
  },
  {
    text: 'Бонусы партнерской программы',
    img: dollars,
  },
];

type Props = {
  img: string;
  text: string;
};

const Advantage: React.FC<Props> = ({ text, img }) => (
  <div className={styles.advantage}>
    <img src={img} alt='icon' />
    <h4>{text}</h4>
  </div>
);

export const Advantages: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.container, className)}>
    {data.map(item => (
      <Advantage key={item.img} {...item} />
    ))}
  </div>
);
