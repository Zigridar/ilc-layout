import React from 'react';
import styles from './circle.module.scss';
import cn from 'classnames';

export const Circle: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.circle, className)}>
    <p>Доход</p>
    <p>до $260</p>
    <p>с каждого полиса</p>
  </div>
);
