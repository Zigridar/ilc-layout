import React from 'react';
import styles from './text.module.scss';
import { Button } from '../../../../button/Button.tsx';
import cn from 'classnames';

export const TextBlock: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.text, className)}>
    <h1 className={styles.main}>
      Юридический полис ILC — быстрое решение сложных вопросов 24/7
    </h1>
    <p className={styles.secondary}>
      Ваша личная команда профессиональных юристов, которая всегда под рукой. А
      также доступ к бонусам партнерской программы — доход до $260 с каждого
      полиса, купленного по вашему приглашению.
    </p>
    <Button className={styles.button}>Попробовать бесплатно</Button>
  </div>
);
