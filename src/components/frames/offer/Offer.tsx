import React from 'react';
import styles from './offer.module.scss';
import { Header } from './components/header/Header.tsx';
import { Cards } from './components/cards/Cards.tsx';
import { Start } from './components/start/Start.tsx';

export const Offer: React.FC = () => (
  <section>
    <Header className={styles.header} />
    <Cards className={styles.cards} />
    <Start className={styles.start} />
  </section>
);
