import React from 'react';
import cn from 'classnames';
import preferences from '../../../../../assets/images/preferences.png';
import gift from '../../../../../assets/images/gift.png';
import education from '../../../../../assets/images/education.png';
import development from '../../../../../assets/images/development.png';
import styles from './cards.module.scss';
import { Button } from '../../../../button/Button.tsx';

type Card = {
  img: string;
  title: string;
  content: React.ReactNode;
};

const cards: Array<Card> = [
  {
    title: 'Высокие бонусы',
    img: gift,
    content: (
      <>
        <p>
          Вы получаете до 35% от стоимости каждого полиса, который был куплен по
          вашей рекомендации.
        </p>
        <p className={cn(styles.gray, styles.offsetTop)}>
          К примеру: юридический полис “Международный бизнес” стоит $750/год, с
          него вы зарабатываете $260.
        </p>
      </>
    ),
  },
  {
    title: 'Полезный продукт',
    img: preferences,
    content: (
      <>
        Сервис ILC — это ваша личная команда юристов, которая работает в формате
        “здесь и сейчас”, 24/7, без выходных.
        <p className={styles.offsetTop}>
          Нужна помощь? Всего 1 клик и персональный юрист ILC уже решает ваш
          вопрос.{' '}
          <span className={styles.bold}>
            Это в несколько раз дешевле и быстрее, чем искать частного
            специалиста.
          </span>
        </p>
      </>
    ),
  },
  {
    title: 'Широкая аудитория',
    img: development,
    content: (
      <>
        Качественная юридическая помощь нужна абсолютно всем: бизнесу, семьям,
        крупным компаниям, экспертам и частным лицам.
        <p className={styles.offsetTop}>
          Рекомендуя полис ILC,{' '}
          <span className={styles.bold}>вы помогаете людям </span> оперативно
          решать жизненные задачи в 1 клик.
        </p>
      </>
    ),
  },
  {
    title: 'Обучение и поддержка',
    img: education,
    content: (
      <>
        Вы получите доступ в личный кабинет с подробной информацией о том, как:
        <ul className={styles.list}>
          <li>эффективно привлекать клиентов,</li>
          <li>отслеживать конверсию,</li>
          <li> повышать свой доход,</li>
          <li>обналичивать средства и многое другое.</li>
        </ul>
        <p className={styles.bold}>Мы поддержим вас на всех этапах.</p>
      </>
    ),
  },
];

export const Cards: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.container, className)}>
    <div className={styles.cards}>
      {cards.map(({ title, content, img }) => (
        <div key={title} className={styles.card}>
          <img className={styles.image} src={img} alt='logo' />
          <h4 className={styles.title}>{title}</h4>
          <div className={styles.content}>{content}</div>
        </div>
      ))}
    </div>
    <Button>Стать партнером</Button>
  </div>
);
