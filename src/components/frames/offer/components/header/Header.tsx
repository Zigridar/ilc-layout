import React from 'react';
import cn from 'classnames';
import people from '../../../../../assets/images/people.svg';
import planet from '../../../../../assets/images/planet.png';
import styles from './header.module.scss';

export const Header: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.header, className)}>
    <div className={styles.planet}>
      <img src={planet} alt='planet' />
    </div>
    <div className={styles.people}>
      <img src={people} alt='people' />
    </div>
    <div className={styles.text}>
      <h2 className={styles.title}>Станьте партнером ILC</h2>
      <p className={styles.content}>
        и зарабатывайте на рекомендациях в любое время, из любой точки мира
      </p>
    </div>
  </div>
);
