import React from 'react';
import cn from 'classnames';
import styles from './start.module.scss';
import { Button } from '../../../../button/Button.tsx';

export const Start: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.container, className)}>
    <h2 className={styles.mainTitle}>Начать легко</h2>
    <div className={styles.cards}>
      <div className={styles.card}>
        <h4 className={styles.title}>Регистрация</h4>
        <p className={styles.text}>
          Заполните простую форму для участия в программе и активируйте
          клиентский пакет.
        </p>
      </div>
      <div className={styles.card}>
        <p className={styles.title}>Обучение</p>
        <p className={styles.text}>
          Вас ждут полезные обучающие материалы, с помощью которых вы легко
          сможете монетизировать свое время.
        </p>
      </div>
      <div className={styles.card}>
        <p className={styles.title}>Стратегия</p>
        <p className={styles.text}>
          Воспользуйтесь подробным и понятным алгоритмом действий для достижения
          классных результатов.
        </p>
      </div>
      <div className={styles.card}>
        <p className={styles.title}>Результат</p>
        <p className={styles.text}>
          Зарабатывайте до 35% с каждой покупки полиса по вашей партнерской
          ссылке с первых дней после регистрации.
        </p>
        <Button>Начать зарабатывать</Button>
      </div>
    </div>
  </div>
);
