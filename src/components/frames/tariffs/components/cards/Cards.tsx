import React from 'react';
import cn from 'classnames';
import styles from './cards.module.scss';
import { Triangle } from '../../../../triangle/Triangle.tsx';
import { Button } from '../../../../button/Button.tsx';

type Card = {
  header: string;
  participant: string;
  price: string;
  items: Array<string>;
};

const cards: Array<Card> = [
  {
    header: 'Легкий старт',
    participant: 'Бесплатно',
    price: 'Для ознакомления с сервисом и его возможностями',
    items: [
      'Конструктор документов',
      'Sirius',
      'Личный кабинет (базовая версия)',
      '1 устная первичная консультация по правовому запросу',
    ],
  },
  {
    header: 'Стандарт',
    participant: 'Для физических лиц',
    price: '$28/месяц',
    items: [
      'Бесплатная телефонная линия + функция “перезвоните мне”',
      'Доступ к первому юридическому интеллекту Sirius 24/7',
      'Личный кабинет (полная версия)',
      'Конструктор документов',
      'Устные и письменные консультации юристов 24/7',
    ],
  },
  {
    header: 'бизнес-консультант',
    participant: 'Для частных лиц и фрилансеров',
    price: '$38/месяц',
    items: [
      'Бесплатная телефонная линия + функция «перезвоните мне»',
      'Доступ к первому юридическому интеллекту Sirius 24/7',
      'Пакет документов для открытия бизнеса в РФ',
      'Сопровождение сделок ',
      'Устные и письменные консультации юристов 24/7',
    ],
  },
  {
    header: 'бизнес-премиум',
    participant: 'Для среднего бизнеса',
    price: '$49/месяц',
    items: [
      'Разработка уникальных договоров любой сложности',
      'Доступ к первому юридическому искусственному интеллекту Sirius — 24/7',
      'Регистрация компаний и открытия счетов в РФ',
      'Сопровождение сделок любой сложности',
      'Устные и письменные консультации юристов 24/7',
    ],
  },
  {
    header: 'международный бизнес',
    participant: 'Для ведения бизнеса за рубежом',
    price: '$98/месяц',
    items: [
      'Разработка уникальных договоров любой сложности',
      'Консультации и сопровождение по регистрации компаний и банковских счетов по всему миру',
      'Оказание юридических услуг на английском языке',
      'Консультации по международному праву',
      'Сопровождение сделок любой сложности',
    ],
  },
];

export const Cards: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.cards, className)}>
    {cards.map(({ participant, header, price, items }) => (
      <div key={participant} className={styles.card}>
        <div className={styles.content}>
          <h4 className={styles.header}>{header}</h4>
          <p className={styles.participant}>{participant}</p>
          <p className={styles.price}>{price}</p>
          <p className={styles.available}>Для Вас доступно</p>
          <ul className={styles.items}>
            {items.map(item => (
              <li key={item}>
                <span className={styles.triangle}>
                  <Triangle />
                </span>
                <span>{item}</span>
              </li>
            ))}
          </ul>
        </div>
        <div className={styles.buttons}>
          <a href='#' className={styles.more}>
            {'Подробнее в личном кабинете →'}
          </a>
          <Button className={styles.btn}>Оставить заявку</Button>
        </div>
      </div>
    ))}
  </div>
);
