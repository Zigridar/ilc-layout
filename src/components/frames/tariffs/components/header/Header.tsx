import React from 'react';
import styles from './header.module.scss';
import cn from 'classnames';
import { Button } from '../../../../button/Button.tsx';

export const Header: React.FC<IClassName> = ({ className }) => (
  <div className={cn(styles.header, className)}>
    <h2 className={styles.title}>Тарифы</h2>
    <p className={styles.text}>для всех и для каждого</p>
    <div className={styles.buttons}>
      <Button className={styles.btn}>1 месяц</Button>
      <span>
        <a href='#'>Год</a>
      </span>
    </div>
  </div>
);
