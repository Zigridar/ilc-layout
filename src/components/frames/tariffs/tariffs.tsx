import React from 'react';
import { Header } from './components/header/Header.tsx';
import { Cards } from './components/cards/Cards.tsx';
import styles from './tariffs.module.scss';

export const Tariffs: React.FC = () => (
  <section className={styles.container}>
    <Header className={styles.header} />
    <Cards className={styles.cards} />
  </section>
);
