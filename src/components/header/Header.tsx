import React, { useCallback, useEffect, useState } from 'react';
import styles from './header.module.scss';
import logo from '../../assets/images/logo.svg';
import { Button } from '../button/Button.tsx';
import cn from 'classnames';
import { Navbar } from './components/navbar/Navbar.tsx';
import { MenuButton } from './components/menu-button/MenuButton.tsx';
import { PhoneButton } from './components/phone-button/PhoneButton.tsx';

export const Header: React.FC = () => {
  /** Состояние открытия меню */
  const [open, setOpen] = useState<boolean>(false);
  /** Обработка открытия/закрытия */
  const handleToggle = useCallback(() => setOpen(prev => !prev), []);
  /** Состояние скрола */
  const [scrolled, setScrolled] = useState<boolean>(window.scrollY > 0);

  useEffect(() => {
    const listener = () => {
      const newScrolled = window.scrollY > 0;
      setScrolled(prev => {
        if (prev !== newScrolled) {
          return newScrolled;
        }
        return prev;
      });
    };
    window.addEventListener('scroll', listener);

    return () => window.removeEventListener('scroll', listener);
  }, []);

  return (
    <header
      className={cn(styles.header, {
        [styles.open]: open,
        [styles.scrolled]: scrolled,
      })}
    >
      <div className={styles.wrap}>
        <div className={styles.logo}>
          <div className={styles.imgWrap}>
            <img src={logo} alt='logo' />
          </div>
          <div>Первый цифровой международный юридический центр</div>
        </div>
        <div className={styles.login}>
          <a href='tel:8 (800) 511-37-68' className={styles.phone}>
            8 (800) 511-37-68
          </a>
          <Button className={styles.btn}>Войти</Button>
          <div className={styles.iconBtns}>
            <a href='tel:8 (800) 511-37-68'>
              <PhoneButton />
            </a>
            <MenuButton onClick={handleToggle} open={open} />
          </div>
        </div>
        <Navbar open={open} className={styles.nav} />
      </div>
    </header>
  );
};
