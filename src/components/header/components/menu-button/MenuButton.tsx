import React from 'react';
import cn from 'classnames';
import styles from './menu-button.module.scss';

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  open: boolean;
};

export const MenuButton: React.FC<Props> = ({ open, ...rest }) => (
  <button
    className={cn(styles.button, { [styles.open]: open })}
    type='button'
    {...rest}
  >
    <span />
  </button>
);
