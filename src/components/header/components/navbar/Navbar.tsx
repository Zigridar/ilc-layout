import React from 'react';
import styles from './navbar.module.scss';
import cn from 'classnames';

type NavItem = {
  title: string;
  path: string;
};

const navItems: Array<NavItem> = [
  {
    title: 'О компании',
    path: '',
  },
  {
    title: 'Услуги',
    path: '',
  },
  {
    title: 'Sirius',
    path: '',
  },
  {
    title: 'Тарифы',
    path: '',
  },
  {
    title: 'FAQ',
    path: '',
  },
  {
    title: 'Контакты',
    path: '',
  },
];

type Props = IClassName & {
  open: boolean;
};

export const Navbar: React.FC<Props> = ({ className, open }) => (
  <nav className={cn(styles.nav, className, { [styles.navOpen]: open })}>
    {navItems.map(({ title, path }) => (
      <a onClick={e => e.preventDefault()} href={path} key={title}>
        {title}
      </a>
    ))}
  </nav>
);
