import React from 'react';
import styles from './phone-button.module.scss';
import smartphone from '../../../../assets/images/smartphone.svg';

export const PhoneButton: React.FC = () => (
  <button className={styles.button}>
    <img src={smartphone} alt='smartphone' />
  </button>
);
