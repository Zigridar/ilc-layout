import React from 'react';
import styles from './triangle.module.scss';
import cn from 'classnames';

export const Triangle: React.FC<IClassName> = ({ className }) => (
  <span className={cn(styles.triangleContainer, className)}>
    <span className={styles.triangle}></span>
  </span>
);
